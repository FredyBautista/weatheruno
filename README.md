### To test the project plesase follow the next steps:

Git clone https://gitlab.com/FredyBautista/weatheruno.git or unzip the folder

cd weatheruno
npm install
npm run dev

You can see the page in http://localhost:8080

### How to use it

You will find a form where you can type the name of the City and also you can 
choose the unit for the temparature, click on search and it would be presented
the forecast for that City.