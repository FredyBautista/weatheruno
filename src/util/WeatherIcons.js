export const PATH = '../public/images/';

export const ICONS = {
    'clear sky': 'clear-sky.png',
    cloudy: 'cloudy.png',
    cold: 'cold.png',
    crescent: 'crescent.png',
    'few clouds': 'few-clouds.png',
    'heavy intensity rain': 'heavy-intensity-rain.png',
    'sunny': 'hot-summer.png',
    lightning: 'lightning.png',
    'moderate rain': 'moderate-rain.png',
    rain: 'rain.png',
    'scattered clouds': 'scattered-clouds.png',
    snow: 'snow.png',
    thunderstorm: 'thunderstorm.png',
    'overcast clouds': 'scattered-clouds.png',
    'light rain': 'rain.png',
    'broken clouds': 'scattered-clouds.png'
}

export default {PATH, ICONS};