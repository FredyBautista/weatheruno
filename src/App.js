import React, {Component} from 'react';
import {connect} from 'react-redux';
import SearchForm from './components/Form';
import { Col, Container, Row } from 'shards-react';
import Forecast from './components/Forecast';
import { css } from '@emotion/core';
import { ClipLoader } from 'react-spinners';

class App extends Component {
    state = {
        loading: false
    }

    onClick = ()=> {
        this.setState({ loading: true })
    }

    render(){
        const {list, city, units} = this.props.weather;
        return (      
            <Container>
                <Row>
                <Col sm="12">   
                    <SearchForm onClick={this.onClick}/>
                </Col>
                <div className='sweet-loading'>
                    <ClipLoader sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading && !(list)} />
                </div> 
                { 
                    (list) && <Forecast units={units} list={list} city={city} />
                }
                </Row>
            </Container>
        );
    } 
}

const mapStateToProps = state => state;
export default connect(mapStateToProps)(App);