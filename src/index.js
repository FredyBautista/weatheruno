import React from 'react';
import ReactDOM from 'react-dom';
import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css"
import { Provider } from 'react-redux';
import { configureStore } from './redux/configureStore';

import App from './App';
const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
    ,document.getElementById('app')
);

export { default as Forecast } from './components/Forecast';