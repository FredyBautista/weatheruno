import axios from 'axios';

const APP_ID = 'e3a1007c3f7d3f0248609ceea74236de';
const nameDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

export const getWeather = async (city, units) => {
    const {lat, lon} = city;
    const res = await axios.get(`https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&appid=${APP_ID}&units=${units}`);
    const {list} = res.data;
    res.data.list = getFiveDays(list);
    return res.data;
}

const getFiveDays = (days) => {
    let currentDate = new Date();
    const appendLeadingZeroes = n => (n <= 9) ? `0${n}` : n;
    let fiveDays = [];    
    for(let x=0; x<5; x++){
        currentDate.setDate(currentDate.getDate() + 1);
        let formatted_date = `${currentDate.getFullYear()}-${appendLeadingZeroes(currentDate.getMonth() + 1)}-${appendLeadingZeroes(currentDate.getDate())} 12:00:00`;
        const day = days.find(day => day.dt_txt == formatted_date);
        day.weather = day.weather[0];
        day.nameDay = nameDays[currentDate.getDay()];
        fiveDays.push(day);
    }
    return fiveDays; 
}

export default getWeather;