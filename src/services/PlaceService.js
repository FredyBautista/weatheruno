import axios from 'axios';

export const getPlaceLatLon = async place => {
    const encodeURL = encodeURI(place);
    const instance = axios.create({
        baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${encodeURL}`,
        headers: {'X-RapidAPI-Key': 'e37eb451a0mshb9ca6644ca0b8dap106743jsn56b6913b4f85'}
    });

    const res = await instance.get();
    const data = res.data.Results;
    if(data.length === 0) {
        throw new Error(`No results for ${place}`);
    }
    const {name: address, lat, lon} = data[0];
    return {
        address,
        lat,
        lon
    }
}

export default getPlaceLatLon;