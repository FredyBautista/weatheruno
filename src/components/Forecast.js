import React from 'react';
import { Col, Card, CardBody, CardImg, Row, CardHeader } from "shards-react";
import {PATH, ICONS} from '../util/WeatherIcons';

const Forecast = (props) => {
    const {units, list, city} = props;
    const unit = (units == 'imperial') ? '°F' : '°C';
    return (
            <Row>
                <Col sm="12">
                    <h2>Forecast for {city}</h2>
                </Col>
            {
                list.map((d, index) => {
                    return (
                    <Col sm="5" lg="2" key={`day-${index}`}>
                        <Card style={{ width: "25 em" }} small={true}>
                            <CardHeader><strong>{d.nameDay}</strong></CardHeader>
                            <CardImg className='icon-weather' top src={`${PATH}${ICONS[d.weather.description]}`} />
                            <CardBody>
                         
                                <label>{d.weather.description}</label>
                                <label>Max Temp: {d.main.temp_max} {unit}</label> 
                                <label>Min Temp: {d.main.temp_min} {unit}</label> 
                           
                            </CardBody>
                        </Card>
                    </Col>
                    )
                })
            }
        </Row>
    )
} 

export default Forecast;