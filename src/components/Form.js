import React, {Component} from 'react';
import { Form, FormGroup, FormInput, Button, FormRadio,InputGroup, Alert } from "shards-react";
import {connect} from 'react-redux';
import {getWeatherAction} from '../redux/actions/WeahterAction';

class SearchForm extends Component {
    constructor(){
        super();
        this.state = {
            City: '',
            forecast: {},
            selectedUnit: '',
            visible: false
        }
    }

    onChange = (event) => {
        this.setState({
          City: event.target.value
        });
    }
    
    onClick = () => {
        const {City, selectedUnit} = this.state;
        const place = { city: City, units: selectedUnit }
        if(City !== '' && selectedUnit !== '') {
            this.props.setPlace(place);
            this.props.onClick();
        }else {
            this.setState({ visible: true })
        }
    }

    changeUnit = (units) => {
        this.setState({
            selectedUnit: units
        });
    }

    dismiss = () => {
        this.setState({ visible: false });
    }

    render(){
        const { selectedUnit, City, show} = this.state;
        return(
            <div>
                <Form>              
                    <FormGroup>
                        <InputGroup>
                            <FormInput onChange={this.onChange} id="city" placeholder="City"/>
                            <FormGroup>
                                <FormRadio inline name="units" checked={selectedUnit == 'metric'} onChange={this.changeUnit.bind(this,'metric')}>
                                °C
                                </FormRadio>
                                <FormRadio inline name="units" checked={selectedUnit == 'imperial'} onChange={this.changeUnit.bind(this,'imperial')}>
                                °F
                                </FormRadio>
                            </FormGroup>
                        </InputGroup><br/>
                        <Button onClick={this.onClick}>Search</Button>
                    </FormGroup>
                </Form>
                <Alert theme="danger" dismissible={this.dismiss} open={this.state.visible}>
                    You need fill the inputs
                </Alert>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        city: state.City,
        units: state.selectedUnit
    }
};
const mapDispatchToProps = dispatch => ({
    setPlace: place => dispatch(getWeatherAction(place))
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchForm);