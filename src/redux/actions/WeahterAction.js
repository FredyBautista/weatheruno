import {getPlaceLatLon} from '../../services/PlaceService'; 
import {getWeather} from '../../services/WeatherService';

export const getWeatherAction = action => {
    return dispatch => {
        dispatch(getWeatherStarted());
        getPlaceLatLon(action.city)
        .then(res =>{
            getWeather(res, action.units).then(weather => {
                dispatch(getWeatherSuccess(res, weather, action))
            })
        })
        .catch(error => {
            dispatch(getWeatherFailure(error))
        })
    }
}

const getWeatherSuccess = (res, weather, action) => {
    return {
        type: "SUCCESS",
        payload: {...res, ...weather, ...action}
    }
}

const getWeatherStarted = () => ({
    type: "STARTED"
})

const getWeatherFailure = error => ({
    type: "FAILURE",
    payload: {...error}
})
