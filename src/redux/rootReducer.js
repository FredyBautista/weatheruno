import { combineReducers } from 'redux';
import {weatherReducer} from './reducers/WeatherReducer'

export const rootReducer = combineReducers({
    weather: weatherReducer
});
