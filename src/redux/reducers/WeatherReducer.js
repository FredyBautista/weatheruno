const initialState = {
    city: '',
    units: 'imperial'
}
export const weatherReducer = (state = initialState, action) => { 
    switch(action.type){
        case "STARTED":
            return {
                ...state,
                loading: true
            };
        case "SUCCESS":
            return {
                ...state,
                loading: false,
                error: false,
                ...action.payload
            }
        default:
            return {
                ...state,
                loading: false,
                error: true
            }
    }
}
